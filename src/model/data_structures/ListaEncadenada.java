package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> {
	
	//atributos//
	private NodoSencillo<T> primero;
	private NodoSencillo<T> actual;
	private int posactual;
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub crear una clase que implemente iterator 
		return null;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		if(primero == null)
		{
			primero = new NodoSencillo<T>(elem);
			actual = primero;
			posactual = 1;
		}
		else
		{
			NodoSencillo<T> ns = primero;
			while(ns.darSiguiente() != null)
			{
				ns = ns.darSiguiente();
			}
			ns.cambiarSiguiente(new NodoSencillo<T>(elem));
		}
	}

	@Override
	public T darElemento(int pos) {
		
		if(posactual > pos)
		{
			while(posactual > pos)
			{
				retrocederPosicionAnterior();
			}
		}
		else if(posactual < pos)
		{
			while(posactual < pos)
			{
				avanzarSiguientePosicion();
			}
		}
		return actual.darElemento();
	}


	@Override
	public int darNumeroElementos() {
		
		if(primero == null)
		{
			return 0;
		}
		int contador = 0;
		NodoSencillo<T> aux = primero;
		while(aux.darSiguiente() != null)
		{
			contador++;
		}
		return contador;
	}

	@Override
	public T darElementoPosicionActual() {
		return actual.darElemento();
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		if(actual.darSiguiente() != null)
		{
			actual = actual.darSiguiente();
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		if()
		return false;
	}

}

//CLASE DEL NODO SENCILLO
 class NodoSencillo<T> 
{
	private T elemento;
	private NodoSencillo<T> siguiente;
	
	public NodoSencillo(T elem)
	{
		elemento = elem;

	}
	 
	public T darElemento()
	{
		return elemento;
	}
	
	public void setElemento(T pElemento)
	{
		elemento = pElemento;
	}
	
	public NodoSencillo<T> darSiguiente ()
	{
		return siguiente;
	}
	
	public void cambiarSiguiente (NodoSencillo<T> elem)
	{
		siguiente = elem;
	}
	

}
